#include <iostream>
using namespace std;
int main()
{
    short int A[20];
    short int n;
    short int min = 0;
    int I1;
    cout << "n=";
    cin >> n;
    int i = 0;
    while (i != n) {
        cin >> A[i];
        i++;
    }
    __asm {

        Mov EAX, 0
        MOV ECX, 0
        mov CX, n
        MOV ESI, 0
        LC :
        push CX
            mov AX, A[ESI]
            MOV EBX, ESI
            Lmin :
        cmp A[EBX], AX
            JG Lmin2
            MOV AX, A[EBX]
            mov min, AX
            MOV I1, EBX
            Lmin2 :
        ADD EBX, 2
            loop Lmin
            PUSH A[ESI]
            MOV EDI, I1
            PUSH A[EDI]
            POP A[ESI]
            POP A[EDI]
            POP CX
            ADD ESI, 2
            LOOP LC
    }
    i = 0;
    while (i != n) {
        cout << A[i] << "\t";
        i++;
    }

    return 0;
}

