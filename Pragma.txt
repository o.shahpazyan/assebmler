#include <iostream>
#include <chrono>
#include <cstdlib>
using namespace std;
double  start_time;
double end_time;

void ParallelBubbleSort(int* ar, int size)
{
	auto t1 = std::chrono::high_resolution_clock::now();
	for (int i = 0; i < size; i++)
	{
		int v = 0;
		if (i % 2 == 0)
		{
		#pragma omp parallel for
			for (int j = 0; j < size; j += 2)
				if (j < size - 1)
					if (ar[j] > ar[j + 1])
					{
						v = ar[j];
						ar[j] = ar[j + 1];
						ar[j + 1] = v;
					}
		}
		else
		{
		#pragma omp parallel for 
			for (int j = 1; j < size; j += 2)
				if (j < size - 1)
					if (ar[j] > ar[j + 1])
					{
						v = ar[j];
						ar[j] = ar[j + 1];
						ar[j + 1] = v;
					}
		}
	}
	auto t2 = std::chrono::high_resolution_clock::now();
	long long generationDuration = std::chrono::duration_cast<std::chrono::microseconds>(t2 - t1).count();
	std::cout << "Generation (mcs): " << generationDuration / 1000000. << std::endl;
}
int main()
{
	srand(0);
	int* arr;
	int size;
	cout << "n = ";
	cin >> size;
	arr = new int[size];
	for (int i = 0; i < size; i++) {
		//cout << "arr[" << i << "] = ";
		arr[i] = rand() % 100;
		//cout << arr[i];
	}
	ParallelBubbleSort(arr, size);
	//for (int i = 0; i < size; i++) {
	//    cout << arr[i] << " ";
	//}
	cout << endl;
	delete[] arr;
}